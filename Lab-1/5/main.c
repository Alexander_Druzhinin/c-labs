# include <stdio.h>

int main () {
    int foot, inch, extrafoot, metr;
    float metric, sm;
    puts ("Put the height in foot and inches");
    scanf ("%d%d", &foot, &inch);
    printf ("%d   %d \n", foot, inch);
    if (inch >= 12) {
        extrafoot = inch / 12;
        inch = inch % 12;
        foot += extrafoot;
        puts ("The inches was set incorrectly, the programm will correct information");
        printf ("%d  %d \n", foot, inch);
    }
    metric = foot*12*2.54 + inch*2.54;
    printf ("The height in european scales is %.1f sm \n \n", metric);
    metr = metric / 100;
    sm = metric - 100*metr;
    printf ("The height in european scales is %d m. and %.1f sm. \n \n", metr, sm);
    return 0;
}
