# include <stdio.h>

int main () {
    int hours, min, sec;
    puts ("Put hours, minutes and seconds");
    scanf ("%d%d%d", &hours, &min, &sec);
    if (hours < 0 || min < 0 || sec < 0 || hours > 24 || min > 59 || sec > 59) {
        puts ("Something is incorrect!");
        return 1;
    }
    if (hours < 5 || hours > 21)
        puts ("Good night!");
    else if (hours >= 5 && hours < 10)
        puts ("Good morning!");
    else if (hours >= 10 && hours < 17)
        puts ("Good afternoon!");
    else if (hours >= 17 && hours < 22)
        puts ("Good evening!");

    return 0;
}
