# include <stdio.h>

int main () {
    char indicator;
    float result;
    float value;
    const float pi = 3.141592f;
    while (1) {
        puts ("Set the value in format 0.00X (0 - exit)");
        scanf ("%f%c", &value, &indicator);
        if (value == 0) break;
        printf ("%c \n", indicator);
        if (indicator == 'D') {
            result = pi / 180 * value;
            printf ("You set the value in degrees. In radians it is %f \n", result);
        }
        else if (indicator == 'R') {
            result = 180 / pi * value;
            printf ("You set the value in radians. In degrees it is %.2f \n \n", result);
        }
        else
            puts ("The type of value is incorrect \n");
        value = 0;

    }
        return 0;
}
