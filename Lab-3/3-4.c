# include <stdio.h>
# include <time.h>

// A big quastion about variable in the sizes of arrays
void setArray (int array [], int size) // Making the array with the same quantity of positive and negative numbers
{
    int indicator = 1, positive, negative, i;
    while (indicator)
    {
        positive = negative = 0;
        for (i = 0; i < size; i++) {
            array [i] = -10 + rand () % 21;
            if (array [i] > 0)
                positive++;
            else
                negative++;
            if (positive > negative + 1 || negative > positive + 1) {
                indicator = 1;
            }
            else
                indicator = 0;
            }
    }
}

void printArray (int array [], int size)
{
    int i;
    for (i = 0; i < size; i++) {
        printf ("%d ", array [i]);
    }
    putchar ('\n');
}

int target (int array [], int size)
{
    int i, j, firstNeg, lastPos, sum = 0; // Calculating first negative and last positive number and their positions
    for (i = 0; i < size; i++) {
        if (array [i] < 0) {
            firstNeg = array [i];
            break;
        }
    }
    for (j = size - 1; j > 0; j--) {
        if (array [j] > 0) {
            lastPos = array [j];
            break;
        }
    }
    if (i < j) {  // Comparing positions of nesessary words in array and calculating the sum
        i += 1;
        while (i < j)
            sum += array [i++];
    }
    else {
        j += 1;
        while (j > i)
            sum += array [j++];
    }
    return sum;
}


int main ()
{
    int size = 10, array [size], result, sum;
    puts ("Input the size of array (min 3, max 20)");
    result = scanf ("%d", &size);
    srand (time(0));
    if (result < 1 || size < 3 || size > 20) {
        puts ("Your value is incorrect");
        return 11;
    }
    else {
        setArray (array, size);
        printArray (array, size);
        printf ("\nThe sum between first negative and last positive numbers is %d\n", target (array, size));
    }
    return 0;
}
