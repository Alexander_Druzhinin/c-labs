# include <stdio.h>
# include <time.h>

# define N 10

int main () {
    int array [N], i, *min = array, *max = array, sum = 0;
    srand (time(0));
    for (i = 0; i < N; i++) {
        array [i] = rand() % 100;
        printf ("%d ", array [i]);
    }
    putchar ('\n');
    int ;
    for (i = 1; i < N; i++) {  // Finding the adresses of min and max elements
        if (array [i] < *min)
            min = &array [i];
        if (array [i] > *max)
            max = &array [i];
    }
    printf ("\nmin - %d, max - %d \n", *min, *max);
    if (min < max) // If min is situated before max
    {
        sum -= *min;
        while (min < max) {
            sum += *min++;
           // min++;
        }
    }
    else
    {
        sum -= *max;
        while (max < min) {
            sum += *max++;
            //max++;
        }
    }
    printf ("The sum between min and max elements is %d \n", sum);
    return 0;
}
