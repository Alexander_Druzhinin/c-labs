# include <stdio.h>
# include <string.h>

int countingWords (char str []) {
    int i = 0, inWord = 0, countWord = 0;
    while (i < strlen (str) - 1) {
        if (inWord == 0 && str [i] != ' ')
        {
            inWord = 1;
            countWord ++;
        }
        else if (inWord == 1 && (str [i] == ' ' || str [i] == '\n'))
        {
            inWord = 0;
        }
        i++;
    }
    return countWord;
}


void selectWord (char str [], int numberOfWords) {
    int result, number, inWord = 0, i = 0, counter = 1, countWord = 0;
    puts ("Choose the number of a word");
    result = scanf ("%d", &number);
    if (result < 1 || number < 1 || number > numberOfWords) {
    puts ("The input number is incorrect");
    }
    else {
        while (i < strlen (str) - 1) {
            if (inWord == 0 && str [i] != ' ')
            {
                inWord = 1;
                countWord ++;
                if (countWord == number)
                    putchar (str [i]);
            }

            else if (inWord == 1 && str [i] != ' ')
            {
                if (countWord == number)
                    putchar (str [i]);
            }

            else if (inWord == 1 && (str [i] == ' ' || str [i] == '\n'))
            {
                inWord = 0;
            }
            i++;
        }
    }
}


int main () {
    char str [256];
    int numberOfWords;
    puts ("Set the string");
    fgets (str, 256, stdin);
    numberOfWords = countingWords (str);
    printf ("Number of words is %d\n", numberOfWords);
    selectWord (str, numberOfWords);
    return 0;
}
