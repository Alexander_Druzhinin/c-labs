# include <stdio.h>
# include <string.h>
# include <stdlib.h>

# define N 256
# define NP 4 //maximum of numbers positions

void chomp (char *arr) {
    if (arr [strlen (arr) - 1] == '\n')
        arr [strlen (arr) - 1] = ' ';
}

int main () {
    char arr [N], buf [N];
    int length, i, j, sum = 0, inNumber = 0;

    puts ("Put the string");
    fgets (arr, 256, stdin);

    chomp (arr);

    for (i = 0, j = 0; i < strlen (arr); i++) {

        if (arr [i] >= '0' && arr [i] <= '9' && inNumber == 0) {
            length = inNumber = 1;
            buf [j] = arr [i];
            j++;
        }

        else if (arr [i] >= '0' && arr [i] <= '9' && inNumber == 1) {
            length++;
            buf [j] = arr [i];
            j++;
            if (length == NP) {
                sum += atoi (buf);
                inNumber = 0;
                j = 0;
            }
        }

        else if ((arr [i] < '0' || arr [i] > '9') && inNumber == 1) {
            inNumber = 0;
            buf [j] = 0;
            j = 0;
            sum += atoi (buf);
        }
    }
    printf ("\n The sum in this sequence is %d (max numbers of positions are %d)", sum, NP);
    return 0;
}
