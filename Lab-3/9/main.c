# include <stdio.h>
# include <string.h>

void chomp (char arr []) {
    if (arr [strlen (arr) - 1] == '\n')
       (arr [strlen (arr) - 1] = ' ');
    }


int countingWords (char str []) {
    int i = 0, inWord = 0, countWord = 0;
    while (i < strlen (str) - 1) { // counting number of the words
        if (inWord == 0 && str [i] != ' ')
        {
            inWord = 1;
            countWord ++;
        }
        else if (inWord == 1 && (str [i] == ' ' || str [i] == '\n'))
        {
            inWord = 0;
        }
        i++;
    }
    printf ("\nThe number of words is %d\n", countWord);
    return countWord;
}

int selectWord (char str [], int numberOfWords) {
    int result, number;
    puts ("Choose the number of the cutting word");
    result = scanf ("%d", &number);
    if (result < 1 || number < 1 || number > numberOfWords) {  //checking correct input of putting word
        puts ("The input number is incorrect");
        return 0;
    }
    return number;
}

int lengthOfChosenWord (char str [], int number) {
    int inWord = 0, inThisWord = 0, i, counter = 0, length = 1;
    for (i = 0; i < strlen (str) - 1; i++) {
        if (inWord == 0 && str [i] != ' ') {
            inWord = 1;
            counter++;
            if (counter == number)
                inThisWord = 1;
        }
        else if (inWord == 1 && str [i] != ' ' && str [i] != '\n' && inThisWord == 1)
            length++;
        else if (inWord == 1 && (str [i] == ' ' || str [i] == '\n'))
            inWord = inThisWord = 0;
    }
    return length;
}

void cuttingWord (char str [], int number, int length) {
    int inWord = 0, i, j, wordNumber = 0;
    for (i = 0; i < strlen (str) - 1; i++) { // We are looking for our word
        if (inWord == 0 && str [i] != ' ') { // While we havn't found it, we dont touch at our array
            inWord = 1;
            wordNumber ++;
            if (wordNumber == number) { // We have found it! Let's start to move symbols!
                break;
            }
        }
        else if (inWord == 1 && str [i] == ' ') {
            inWord = 0;
        }
    }
    if (i > 0)
        i -= 1;   // We begin cutting from the space before word, because we don't need double spaces around cutted word
        while (length + 1) {  // How many times we will move other array to left by one symbol
            for (j = i; j < strlen (str) - 1; j++) { //+1 because we moved 'i' to one step before
                str [j] = str [j+1]; // Moving eache symbol to left
              }
            length--;
        }
}

int main () {
    char str [256];
    int choice;
    puts ("Set the string");
    fgets (str, 256, stdin);
    chomp (str);
    choice = selectWord (str, countingWords (str));
    if (choice == 0)
        return 1;
    else
        cuttingWord (str, choice, lengthOfChosenWord (str, choice));
        putchar ('\n');
        puts (str);
        return 0;
}
