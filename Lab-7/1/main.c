# include <stdio.h>
# include <stdlib.h>
# include <string.h>

# define N 128

struct WORD {
    int count;
    char arr [N];
    struct WORD *left, *right;
};

struct WORD *addTree (struct WORD *root, char *add) {
    if (root == NULL) {
        root = (struct WORD*) malloc (sizeof (struct WORD));
        strcpy (root->arr, add);
        root->count = 0;
        root->right = root->left = NULL;
    }
    else if (strcmp (root->arr, add) < 0)
        root->right = addTree (root->right, add);
    else if (strcmp (root->arr, add) > 0)
        root->left = addTree (root->left, add);
    return root;
}

void printTree (struct WORD *root) {
    if (root) {
        printTree (root->left);
        printf ("%s - %d\n", root->arr, root->count);
        printTree (root->right);
    }
}

void searchWord (struct WORD *root, char *word) {
    int cond;
    if ((cond = strcmp (root->arr, word)) == 0)
        root->count++;
    else if (cond > 0 && root->left != NULL)
        searchWord (root->left, word);
    else if (cond < 0 && root->right != NULL)
        searchWord (root->right, word);
    else return;
}

void chomp (char *arr) {
    if (arr [strlen(arr) - 1] == '\n')
        arr [strlen(arr) - 1] = 0;
}

int main () {
    struct WORD *root = NULL;
    char buf [N], s, buf2 [N] = {0};
    int inWord = 0, i = 0, j;
    FILE *fp = fopen("C:\\words.txt", "rt"), *fp2 = fopen ("C:\\program.c","rt");
    if (!fp || !fp2) {
        puts ("The files aren't found");
        return 1;
    }
    while (!feof (fp))
    {
        fscanf (fp, "%s", buf);
        chomp (buf);
        root = addTree (root, buf);
    }


    while ((s = fgetc (fp2)) != -1)
    {

        if (s >= 'a' && s <= 'z') {
            buf2 [i] = s;
            inWord = 1;
            i++;
        }
        else if ((s < 'a' || s > 'z') && inWord == 1) {
            //puts (buf2);
            searchWord (root, buf2);
            i = 0;
            inWord = 0;
            for (j = 0; j < N; j++)
                buf2[j] = 0;

        }

    }

    printTree (root);
    return 0;
}
