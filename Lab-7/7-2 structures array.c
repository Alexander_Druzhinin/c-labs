# include <stdio.h>
# include <stdlib.h>

# define N 256 //Max size of lexicon

struct SYM {
    char symbol;
    int counter;
    float freq;
};

int compare(struct SYM *sym1, struct SYM *sym2)
{
    if ((sym1->freq) < (sym2->freq))
        return 1;
    else
        return -1;
}

int procSymbol (struct SYM *symbols, char symbol) {
    int i;
    static int count = 0;
    for (i = 0; i <= count; i++)
        if (symbols[i].symbol == symbol) {
            symbols[i].counter++;
            return count;
        }
        symbols[count].symbol = symbol;
        symbols[count].counter = 1;//! ������ ����� ���� �����, � �� ���������!?!?!?!?
        symbols[count].freq = 0;
        return ++count;
}

void printStructure (struct SYM *symbols, int count) {
    int i;
    for (i = 0; i < count; i++) {
        if (symbols[i].symbol == '\n')
            printf ("ENTER was used %d times. Frequency is %.3f\n", symbols[i].counter, symbols[i].freq);
        else if (symbols[i].symbol == ' ')
            printf ("SPACE was used %d times. Frequency is %.3f\n", symbols[i].counter, symbols[i].freq);
        else
            printf ("    %c was used %d times. Frequency is %.3f\n", symbols[i].symbol, symbols[i].counter, symbols[i].freq);
    }
}

void calcFrequency (struct SYM *symbols, int equal, int count) {
    int i;
    for (i = 0; i < count; i++) {
        symbols[i].freq = (float) symbols[i].counter / (float) equal;
    }
}

void checkFrequency (struct SYM *symbols, int count) {
    int i;
    float sum = 0;
    for (i = 0; i < count; i++)
        sum += symbols[i].freq;
    printf ("\nEqual frequency = %.3f\n\n", sum);
}

void sortStructures (struct SYM *symbols, int count) {
    qsort (&symbols[0], count, sizeof (struct SYM), compare);
}

int main (int argc, char *argv[]) {
    char s;
    int checker = 0;
    int count, equal = 0; // Count number of symbols in the file from alphabet, equal - all simbols in sum
    FILE *fp = fopen (argv[1], "rt");
    s = fopen (argv[1], "rt");
    struct SYM *symbols;
    if (!fp) {
        puts ("There is no file!");
        return 1;
    }

    symbols = (struct SYM*) calloc (N, sizeof(struct SYM));
    if (!symbols) {
        puts ("The memory hasn't been allocated");
        return 2;
    }

    while ((checker = s = fgetc (fp)) != -1) {
        putchar (s);
        count = procSymbol (symbols, s);
        equal++;
    }

    printf ("\nEqual %d symbols, letters used - %d\n\n", equal, count);

    calcFrequency (symbols, equal, count);
    sortStructures (symbols, count);
    printStructure (symbols, count);
    checkFrequency (symbols, count);

    return 0;
}
