# include <stdio.h>
# include <stdlib.h>
# include <time.h>

# define H 10
# define L 15
# define DELAY 100

void findExit (char (*arr)[L], int a, int b, int x, int y) { // a ann b - location now; x and y - last location
    system ("cls");
    int i, j, now;
    arr [a] [b] = 1;

    if (a == 0 || a == H - 1 || b == 0 || b == L - 1) {
        for (i = 0; i < H; i++) {
            for (j = 0; j < L; j++)
                putchar (arr [i] [j]);
            putchar ('\n');
        }
        arr [a] [b] = 1;
        puts ("\n\nExit is found!");
        return;
    }

    else {
        for (i = 0; i < H; i++) {
            for (j = 0; j < L; j++)
                putchar (arr [i] [j]);
            putchar ('\n');
        }

        now = clock();
        while(clock() < now + DELAY);
        //putchar ('\a');

        if (arr [a+1] [b] == ' ') {
            arr [a] [b] = '.';
            findExit (arr, a + 1, b, a, b);
        }
        else if (arr [a-1] [b] == ' ') {
            arr [a] [b] = '.';
            findExit (arr, a - 1, b, a, b);
        }
        else if (arr [a] [b+1] == ' ') {
            arr [a] [b] = '.';
            findExit (arr, a, b + 1, a, b);
        }
        else if (arr [a] [b-1] == ' ') {
            arr [a] [b] = '.';
            findExit (arr, a, b - 1, a, b);
        }

        else {
            if (arr [a-1][b] == '.' && a-1 != x) {
                arr [a] [b] = '.';
                findExit (arr, a-1, b, a, b);
            }
            else if (arr [a+1][b] == '.' && a+1 != x) {
                arr [a] [b] = '.';
                findExit (arr, a+1, b, a, b);
            }
            else if (arr [a][b+1] == '.' && b+1 != y) {
                arr [a] [b] = '.';
                findExit (arr, a, b+1, a, b);
            }
            else if (arr [a][b-1] == '.' && b-1 != y) {
                arr [a] [b] = '.';
                findExit (arr, a, b-1, a, b);
            }
            else {
                arr [a][b] = '!';  //the dead end!
                findExit (arr, x, y, a, b);
            }
        }
    }

}

int main () {
    int i, j;
    char arr [H] [L] = {
        {'#','#','#','#','#','#','#','#','#','#','#','#','#','#','#'},
        {' ',' ',' ',' ',' ','#',' ',' ',' ',' ','#',' ',' ',' ','#'},
        {'#',' ','#',' ','#','#',' ','#','#','#','#',' ','#',' ','#'},
        {'#',' ','#',' ','#','#',' ','#',' ',' ',' ',' ','#',' ','#'},
        {'#',' ','#',' ','#','#',' ','#','#',' ','#','#',' ',' ','#'},
        {'#',' ','#',' ','#','#',' ','#','#',' ','#','#',' ','#','#'},
        {'#',' ',' ',' ',' ',' ',' ',' ','#',' ','#',' ',' ','#','#'},
        {'#',' ','#','#','#','#','#','#','#',' ','#',' ','#','#','#'},
        {'#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','#'},
        {'#','#','#','#','#','#','#','#','#','#','#','#','#','#','#'}};
    findExit (arr, 3, 9, 3, 9);


    return 0;
}
