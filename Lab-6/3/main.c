# include <stdio.h>
# include <string.h>
# include <stdlib.h>

# define N 256

void cutBrackets (char *arr) {
    char buf [N] = {0}, *p, *q;
    if (arr[0] == '(' && arr [strlen(arr) - 1] == ')')
    {
        p = arr;
        q = buf;
        arr [strlen (arr) - 1] = 0;
        p++;
        while (*p != 0) {
            *q++ = *p++;
        }
    }
    strcpy (arr, buf);
}

char partition (char *arr, char *p, char*q) {
    char *a = arr, operand, *b;
    int countl = 1, countr = 0;  // They count '(' and ')'. '(' = 1 because we must start with difference sums
    if (arr [0] != '(') // if the strig has brackets after operands or hasn't brackets. For example, "x + y" or "x + (y + z)"
    {
        while (1) {
            if (*a == '+' || *a == '-' || *a == '*' || *a == '/')
                break;
            *p++ = *a++;
        }
        operand = *a;
        a++;
        while (*a != 0)
            *q++ = *a++;
    }


    else if (arr [0] == '(' && arr [strlen (arr) - 1] == ')') // (x+y)-(z*w)
    {
        *p++ = *a++;
        while (countl != countr) {
            if (*a == '(')
                countl++;
            if (*a == ')')
                countr++;
            *p++ = *a++;
        }
        operand = *a++;

        while (*a != 0)
            *q++ = *a++;
    }

    else // Example "(x + y) - z"
    {
        a = &arr [strlen (arr) - 1];
        while (1) {
            if (*a == '+' || *a == '-' || *a == '*' || *a == '/')
                break;
            a--;
        }
        operand = *a;

        b = a;

        a++;
        while (*a != 0)
            *q++ = *a++;

        a = arr;

        while (a < b)
            *p++ = *a++;
    }
    return operand;
}

int eval (char *arr) {
    char exp1 [N] = {0}, exp2 [N] = {0}, op, *p, *q;
    int flag = 0, simplef = 0;

    p = arr;
    while (*p != 0) { // If other array has only one number it is simple and we return it!
        if (*p == '+' || *p == '-' || *p == '*' || *p == '/')
            simplef = 1;
        p++;
    }
    if (simplef == 0)
        return atoi (arr);

    else {
        cutBrackets (arr);

        p = arr;
        p = exp1;
        q = exp2;

        op = partition (arr, p, q);

        //puts (exp1);
        //putchar (op);
        //puts (exp2);

        switch (op) {
            case '+':
                return eval (exp1) + eval (exp2);
            case '-':
                return eval (exp1) - eval (exp2);
            case '*':
                return eval (exp1) * eval (exp2);
            case '/':
                return eval (exp1) / eval (exp2);
       }
    }
}

int checker (char *arr) {
    int countl = 1, countr = 0;
    char *p = arr;
    while (*p++ != 0) {
            if (*p == '(')
                countl++;
            if (*p == ')')
                countr++;

    }
    if (countl == countr)
        return 0;
    else
        return 1;
}



int main (int argc, char *argv[]) {
    if (argc != 2) {
        puts ("The program was launched incorrectly");
        return 10;
    }
    char arr [N];
    strcpy (arr, argv [1]);
    if (checker (arr) != 0) {
        puts ("Incorrect string!");
        return 1;
    }
    else {
        printf ("(%s) = %d", arr, eval (arr));
        return 0;
    }
}
