# include <stdio.h>
# include <math.h>
# include <stdlib.h>
# include <time.h>

int getPower () {
    int m;
    puts ("Put the power of 2");
    scanf ("%d", &m);
    return m;
}

int findSize (int m) {
    int n;
    n = pow (2, m);
    printf ("The size of array will be %d\n", n);
    return n;
}

void setArray (int *arr, int sizeArr) {
    int i;
    for (i = 0; i < sizeArr; i++) {
        arr [i] = rand () % 10;
        //printf ("% d", arr [i]);
    }
}

int classicSum (int *arr, int sizeArr) {
    int i, sum = 0;
    for (i = 0; i < sizeArr; i++)
        sum += arr [i];
    return sum;
}

int recursionSum (int *arr, int sizeArr) {
    if (sizeArr == 2)
        //return 6;
        return *arr + arr [sizeArr - 1];
    else {
        return recursionSum (arr, sizeArr / 2) + recursionSum (arr + sizeArr / 2, sizeArr / 2);
    }
}


int main () {
    int main, m, sizeArr;
    clock_t begin, end;
    double clasT, recursT;
    m = getPower ();
    if (m > 25 || m < 1)
        {
        puts ("Incorrect interval");
        return 1;
        }
    else {
        sizeArr = findSize (m);
        int *arr = (int *) malloc (sizeArr*sizeof(int));
        srand (time(0));
        setArray (arr, sizeArr);
        begin = clock ();
        printf ("\nClassic sum is %d\n", classicSum (arr, sizeArr));
        end = clock ();
        clasT = (double) (end - begin) / CLOCKS_PER_SEC;
        printf ("\n%.3f\n", clasT);
        begin = clock ();
        printf ("\nRecursion's way sum is %d\n", recursionSum (arr, sizeArr));
        end = clock ();
        recursT = (double) (end - begin) / CLOCKS_PER_SEC;
        printf ("\n%.3f\n", recursT);
        free (arr);

        printf ("\nThe difference between classical and recursion ways is %.3f sec", recursT - clasT);
        return 0;
    }

}
