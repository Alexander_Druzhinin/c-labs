# include <stdio.h>
# include <string.h>
# include <time.h>
# include <stdlib.h>

# define LENGTH 256

void printWord (int position, char *sequence []) {
    while (1) {
        putchar (*sequence [position]++);
        if (*sequence [position] == ' ' || *sequence [position] == '\n' || *sequence [position] == '\0') {
            putchar (' ');
            break;
        }
    }
}

int getsWords (char arr [], char *pWords []) {   // Making the pointers array
    int inWord = 0, numberOfWords = 0, i = 0, j = 0;
    while (arr [i] != '\0')
    {
        if (inWord == 0 && arr [i] != ' ' && arr [i] != '\0' && arr [i] != '\n')
        {
            inWord = 1;
            pWords [j] = &arr [i];
            j++;
            numberOfWords ++;
        }
        if (inWord == 1 && arr [i] == ' ' || arr [i] == '\0')
            inWord = 0;
        i++;
    }
    return numberOfWords;
}

void swop (char *pWords [], char *sequence [], int number) {
    int i = 0;
    int position;
    for (i = 0; i < number; ) {
        position = rand() % number;
        if (sequence [position] == NULL) {
            sequence [position] = pWords [i];
            i++;
         }
    }
 /*   for (i = 0; i < number; i++) {  Checking of created sequence, printing the first letters from the words
        putchar (*sequence [i]);
        putchar (' ');
    }
    putchar ('\n'); */
}

int main () {
    char arr [LENGTH], *pWords [LENGTH / 2 + 1] = {0}, *sequence [LENGTH / 2 + 1] = {NULL}; // "LENGTH / 2 + 1" - the maximum number of words in the string
    int wordNumber, i;                                                                      // sequence - array with pointer to words in random sequence
    puts ("Put the string");
    fgets (arr, LENGTH, stdin);
    srand (time(0));
    wordNumber = getsWords (arr, pWords);
    swop (pWords, sequence, wordNumber);
    for (i = 0; i < wordNumber; i++) {
        printWord (i, sequence);
    }
    // printf ("\n Number of words is %d", wordNumber);
    return 0;
}
