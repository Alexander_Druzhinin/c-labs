# include <stdlib.h>
# include <stdio.h>
# include <time.h>

# define L 51
# define H 21
# define P 3 //probability of stars (1 in P)
# define MICRODELAY 200 //delay between printing lines
# define DELAY 2000

void setSpaces (char (*arr) [L]){
    int i, j;
    for (i = 0; i < H; i++) {
        for (j = 0; j < L; j++) {
            arr [i] [j] = ' ';
        }
    }
}

void printArray (char (*arr) [L]){
    int i, j, now;
    for (i = 0; i < H; i++) {
        for (j = 0; j < L; j++) {
            putchar (arr [i] [j]);
        }
    putchar ('\n');
    now=clock();
    while(clock()<now+MICRODELAY);
    }
}

void setQuarter (char (*arr) [L]) {
    int i, j, random;
    for (i = 0; i <= H/2; i++) {
        for (j = 0; j <= L/2; j++) {
            random = rand () % P;
            if (random == 0)
                arr [i] [j] = '*';
            else
                arr [i] [j] = ' ';
        }
    }
}

void copyParts (char (*arr) [L]) {
    int i, j, k, l;
    for (i = 0; i <= H/2; i++) {
        for (j = 0, k = L - 1; j <= k; j++, k--) {
            arr [i] [k] = arr [i] [j];
        }
    }

    for (i = 0, l = H - 1; i <= l; i++, l--) {
        for (j = 0; j < L; j++) {
            arr [l] [j] = arr [i] [j];
        }
    }
}


int main () {
    int repeats, res, now;
    char arr [H][L];
    srand (time(0));
    puts ("Set the number of repeats (1 - 100)");
    res = scanf ("%d", &repeats);

    if (res < 1 || repeats < 1 || repeats > 100)
        puts ("The number of repeats is incorrect");
    else {
        while (repeats > 0) {
            setSpaces (arr);
            setQuarter (arr);
            copyParts (arr);
            system ("cls");
            printArray (arr);
            now=clock();
            while(clock()<now+DELAY);
            repeats--;
        }
    }
    return 0;
}
