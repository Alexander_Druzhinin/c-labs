# include <time.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>

# define N 500

void chomp (char *arr) {
    if (arr [strlen (arr) - 1] == '\n' || arr [strlen (arr) - 1] == 'EOF')
        arr [strlen (arr) - 1] = ' ';
}

void swop (char *arr) {
    int i, j, flag, pos, seq [strlen (arr)]; // seq - additional array-queue

    char buf [strlen (arr) - 1]; // buf - copy of a word, later we will puts this with queue sequence
    strcpy (buf, arr);
    srand (time(0));

    for (i = 0; i < strlen (arr); i++) // Initializing sequence array "-1" because it is cant be a number in queue
        seq [i] = -1;

    for (i = 0; i < strlen (arr); ) {   // Making random queue
        flag = 0;
        pos = 0 + rand () % strlen (arr);
        for (j = 0; j < strlen (arr); j++) {
            if (pos == seq [j])
                flag = 1;
        }
        if (flag == 0) {
            seq [i] = pos;
            i++;
        }
    }

    for (i = 0; i < strlen (arr); i++) { // word with random latters
        arr [i] = buf [seq [i]];
    }

    //puts (arr);
}



void getWords (char *str) {
    int inWord = 0, i, j, k, length;
    char *begin, arr [strlen (str) - 2];
    for (i = 0, j = 0; i < strlen (str); i++) {

        if (str [i] != ' ' && inWord == 0) {
            length = inWord = 1;
            begin = &str [i];
        }

        else if (str [i] != ' ' && inWord == 1) {
            length ++;
            arr [j] = str [i];
            j++;
        }

        else if (str [i] == ' ' && inWord == 1) {
            inWord = 0;
            if (length > 2) {
                arr [j-1] = 0;
                length ++;
                swop (arr);
                for (k = 0; k < length - 3; k++) {
                    *(begin + 1) = arr [k];
                    begin++;
                }
            }
            j = 0;
         }
    }
}


int main () {
    char arr [N];
    FILE *fin = fopen ("C:\\list.txt", "rt");
    srand (time(0));
    while (fgets (arr, N, fin)) {
        chomp (arr);
        getWords (arr);
        puts (arr);
    }
    return 0;
}
