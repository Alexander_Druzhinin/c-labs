# include <stdio.h>

# define N 20

void printString (char *p){
    while (*p) {
        putchar (*p++);
    }
}

int main () {
    char name [N][N], *oldest = name, *youngest = name;
    int number, age [N] = {0}, i = 0, *young = age, *old = age, res;
    puts ("How many relatives do you have?");
    res = scanf ("%d", &number);

    if (res < 1 || number < 0 || number > N) {
        puts ("Incorrect values");
        return 1;
    }

    for (i = 0; i < number; i++) {
        printf ("Put the name   ");
        scanf ("%s", name [i]);
        printf ("His (her) age is   ");
        res = scanf ("%d", &age [i]);
        if (res < 1 || age [i] < 0 || age [i] > 150) {
            puts ("Value is incorrect, put his (her) name and age again");
            i--;
        }
        else {
            if (*young > age [i]) {
                young = &age [i];
                youngest = name [i];
            }
            if (*old < age [i]) {
                old = &age [i];
                oldest = name [i];
            }
        }
    }


    printf ("The youngest member is ");
    printString (youngest);
    printf (". He (she) is %d years old\n", *young);

    printf ("The oldest member is ");
    printString (oldest);
    printf (". He (she) is %d years old\n", *old);

    return 0;
}
