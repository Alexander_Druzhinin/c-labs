# include <stdio.h>
# include <string.h>

# define N 256

void chomp (char *arr) {
    if (arr [strlen (arr) - 1] == '\n')
        arr [strlen (arr) - 1] = 0;
}

int main () {
    char arr [N], *pointer, *end, *latter, symbol; //pointer - iterator, end - end of array, latter - symbol in working sequence
    int length = 1, maxLength = 0, i = 0;           //symbol - nesessary symbol in the max sequence
    puts ("Put the string");
    fgets (arr, N, stdin);
    chomp (arr);
    pointer = arr;
    end = pointer + strlen (arr);
    latter = arr;
    for (pointer = arr + 1; pointer < end; pointer++) {
        if (*pointer == *latter) {
            length++;
        }
        else {
            if (length > maxLength) {
                maxLength = length;
                symbol = *latter;
            }
            *latter = *pointer;
            length = 1;
        }
    }
    printf ("Symbol is %c, sequence has %d symbols. This is ", symbol, maxLength);
    for (i = 0; i < maxLength; i++)
        putchar (symbol);
    putchar ('\n');
    return 0;
}
