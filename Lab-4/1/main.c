# include <stdio.h>
# include <stdlib.h>
# include <string.h>

# define M 20
# define N 80

int stringLength (char *p) {
    int lenS = 0;
        while (*p != '\n') {
            lenS ++;
            p++;
            }
    return lenS;
    }

void printString (char *p, int size) {
    while (*p != '\n') {
        putchar (*p);
        p++;
    }
}

int main () {
    char arr [M] [N], *temp, pal = M; // pal - pointers' array length
    int i = 0, j, end, flag;
    puts ("Put the strings (max - 20; empty string - exit)");

    while (i < 20) {
        fgets (arr [i++], N, stdin);
        if (strlen (arr [i - 1]) == 1)
        {
            pal = i - 1; // end of pointer array
            break;
        }
    }

    char *p [pal];
    for (i = 0; i < pal; i++) { // creating the array of pointers
        p [i] = & arr [i] [0];
        }

    for (i = 0; i < pal; i++) { // sorting the array of pointers
        flag = 0;
        for (j = 0; j < pal - 1; j++) {
            if (stringLength (p[j]) > stringLength (p[j+1])) {
                temp = p[j];
                p[j] = p[j+1];
                p[j+1] = temp;
                flag++;
            }
        }
        if (flag == 0)
            break;
    }

    for (i = 0; i < pal; i++) {  // printing strings from the array of pointers
        printString (p [i], pal);
        putchar ('\n');
    }

    return 0;
}
