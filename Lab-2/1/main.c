# include <stdio.h>
# include <time.h>
# include <math.h>

# define DELAY 1000

int main () {
    const float g = 9.81;
    int res;
    float t = 0,h, l, h2;
    clock_t now;
    puts ("Set the height");
    res = scanf ("%f", &h);
    h2 = h;
    if (res < 1 || h < 0 || h > 30000) {
        puts ("The value is incorrect");
    }
    else {
        while (h > 0) {
            printf ("%ct = %2.f sec; \t h = %7.1f \n",'\a', t, h);
            t++;
            l = (g*t*t)/2;
            h = h2 - l;
            now=clock();
            while(clock()<now+DELAY);
        }
        puts ("Booom!");
        t = sqrt (2*h2/g);
        printf ("The time of explosion is %.2f sec\n", t);
    }
    return 0;
}
