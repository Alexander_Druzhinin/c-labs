# include <stdio.h>

int  main () {
    int k, res, i, j;
    puts ("Set the height please");
    res = scanf ("%d", &k);
    if (res < 1 || k < 1 || k > 40)
        puts ("Incorrect!");
    else {
        for (i = 0; i < k ; i++) {
            for (j = 0; j <= 2*k ; j++) {
                if (j < k - i || j > k + i)
                    putchar (' ');
                else
                    putchar ('*');
            }
            putchar ('\n');
        }
    }
    return 0;
}
