# include <stdio.h>
# include <time.h>

int main () {
    int var, user = 0, i, attempt = 0, res = 1; // var - random, user - user's try, attempt - counter of tries
    srand (time(0));
    var = 1 + rand() % 100;
    //printf ("%d \n", var);
    puts ("Press the number (0 - exit)");
    while (var != user) {
        res = scanf ("%d", &user);
        if (res < 1 || user < 0 || user > 100) {
            puts ("Incorrect interval");
            res = 10;
            //break;
        }
        else if (user == 0)
            return 1;

        else if (user < var) {
            puts ("The desired number is bigger, try again!");
            attempt ++;
        }
        else if (user > var) {
            puts ("The desired number is less, try again!");
            attempt ++;
        }

        else {
            attempt++;
            printf ("You have guessed the number with %d attempts! ", attempt);
            break;
        }
    }
    if (attempt == 1 || attempt == 2)
        puts ("You are unbelievable lucky!");
    else if (attempt > 2 && attempt <= 5)
        puts ("You are clever and lucky!");
    else if (attempt > 5 && attempt <= 7)
        puts ("Good result!");
    else
        puts ("You must use logic for better result!");


    return 0;
}
